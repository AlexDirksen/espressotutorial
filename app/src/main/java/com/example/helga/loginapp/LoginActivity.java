package com.example.helga.loginapp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity
{
    // UI references.
    private EditText userExitText;
    private EditText pswEditText;
    private View mProgressView;
    private View mLoginFormView;
    private CheckBox check;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        userExitText = (EditText) findViewById(R.id.EditTextUser);
        userExitText.setFocusable(true);
        userExitText.setFocusableInTouchMode(true);///add this line

        check = (CheckBox) findViewById(R.id.LoginCheck);
        check.setClickable(false);
        pswEditText = (EditText) findViewById(R.id.EditTextPassword);
        pswEditText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.Button_Login);
        mEmailSignInButton.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private boolean attemptLogin()
    {
        // Reset errors.
        userExitText.setError(null);
        pswEditText.setError(null);

        // Store values at the time of the login attempt.
        String email = userExitText.getText().toString();
        String password = pswEditText.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!isPasswordValid(password))
        {
            pswEditText.setError(getString(R.string.error_incorrect_password));
            focusView = pswEditText;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && password.length() < 4)
        {
            pswEditText.setError(getString(R.string.error_invalid_password));
            focusView = pswEditText;
            cancel = true;
        }


        else if (!isUserValid(email))
        {
            userExitText.setError(getString(R.string.error_invalid_user));
            focusView = userExitText;
            cancel = true;
        }

        if (cancel)
        {
            Toast.makeText(LoginActivity.this, "Login failed!", Toast.LENGTH_LONG).show();
            check.setChecked(false);
            return false;
        }
        else
        {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE))
                    .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
            Toast.makeText(LoginActivity.this, "Login succeeded!", Toast.LENGTH_LONG).show();
            check.setChecked(true);
            userExitText.setText("");
            pswEditText.setText("");
            userExitText.requestFocus();
            return false;
        }
    }

    private boolean isUserValid(String user)
    {
        return user.equals("Max");
    }

    private boolean isPasswordValid(String psw)
    {
        return psw.equals("Toaster");
    }
}
